[![pipeline status](https://gitlab.com/iwilfried/vuepress-top-side-nav/badges/master/pipeline.svg)](https://gitlab.com/iwilfried/vuepress-top-side-nav/commits/master)  


### DEMO
https://iwilfried.gitlab.io/vuepress-top-side-nav/  

# VuePress

### The simplicity of this high quality documentation system is the beauty of VuePress.  
`It only takes one README.md markdown formatted file to create and deploy a VuePress site.` 

### Using VuePress is as Easy as 1, 2, 3  
Just watch the demo



`

