module.exports = {
  title: "VuePress ",
  description: "VuePress Project Documentation System",
  base: "/vuepress-top-side-nav/",
  dest: "public",
  themeConfig: {
    lastUpdated: 'Last Updated',
    repo: 'https://gitlab.com/iwilfried/vuepress-top-side-nav',
    docsDir: 'docs',
    docsBranch: 'master',
    editLinks: true,
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'External', link: 'https://google.com' },
      { text: 'Dropdown', items: [
        { text: "Chinese", link: '/chinese/' }, 
        { text: 'Japanese', link: '/japanese/' }
      ]}
    ],
  },
  markdown: {
    // options for markdown-it-anchor

    config: md => {
      // use more markdown-it plugins!
      md.set({ breaks: true })
      md.use(require("markdown-it-anchor")); // Optional, but makes sense as you really want to link to something
      md.use(require("markdown-it-table-of-contents"), {
        includeLevel: [2, 3]
      });
    }
  }
};
